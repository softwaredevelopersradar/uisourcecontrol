﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Input;

namespace UISource
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class RecDroneView : UserControl
    {
        public Dictionary<string, ImageSource> DroneImageCollection;


        //private RecDroneModel _selectedForSave;
        private RecDroneViewModel recDroneViewModel = new RecDroneViewModel();

        #region Events
        public event EventHandler<DronePropertiesModel> OnAddRecord;
        public event EventHandler<DronePropertiesModel> OnChangeRecord;
        public event EventHandler<DronePropertiesModel> OnDeleteRecord;
        public event EventHandler<DronePropertiesModel> OnDeleteTrackRecord;
        public event EventHandler<RecDroneModel> OnAskBearing;
        public event EventHandler<ObservableCollection<RecDroneModel>> OnSaveSource;
        public event EventHandler<(int, DronePropertiesModel)> OnSendToJamming;
        public event EventHandler<DronePropertiesModel> OnSendToRS;
        public event EventHandler<DronePropertiesModel> OnSendToCuirasse;
        public event EventHandler<DronePropertiesModel> OnSendToGrozaR;
        public event EventHandler OnClearRecords;

        

        // Открылось окно с PropertyGrid
        public event EventHandler<DronePropertyView> OnIsWindowPropertyOpen;
        #endregion


        public ObservableCollection<RecDroneModel> Drones
        {
            get => recDroneViewModel.Drones;
            set
            {
                if (recDroneViewModel.Drones == value) 
                    return;

                var newDrones = value;
                var _selectedForSave = recDroneViewModel.DroneSelected;

                recDroneViewModel.Drones.Clear();
                foreach (RecDroneModel recDrone in newDrones)
                {
                    if (recDrone.Type == "No Image")
                    {
                        recDrone.Image = new BitmapImage(new Uri(@"pack://application:,,,/"
                                    + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                                    + ";component/"
                                    + "Images/NoImage1.png", UriKind.Absolute)); 
                    }
                    else
                    {
                        recDrone.Image = DroneImageCollection[recDrone.Type];
                    }
                    recDrone.FormatCoord = FormatViewCoordSource;
                    recDroneViewModel.Drones.Add(recDrone);
                }
                ListDrones.Items.SortDescriptions.Add(new SortDescription("ID",
                                  ListSortDirection.Descending));
                if (_selectedForSave!=null && recDroneViewModel.Drones.Where(t => t.ID == _selectedForSave.ID).FirstOrDefault() != null)
                {
                    recDroneViewModel.DroneSelected = recDroneViewModel.Drones.Where(t => t.ID == _selectedForSave.ID).FirstOrDefault();
                }
            }
        }


        public static readonly DependencyProperty FormatViewCoordSourceProperty = DependencyProperty.Register("FormatViewCoordSource", typeof(CoordFormat), typeof(RecDroneView), new FrameworkPropertyMetadata(CoordFormat.DD_MM_mm));



        public CoordFormat FormatViewCoordSource
        {
            get { return (CoordFormat)GetValue(FormatViewCoordSourceProperty); }
            set { SetValue(FormatViewCoordSourceProperty, value); }
        }

        public DllGrozaSProperties.Models.Languages CLanguage
        {
            get { return (DllGrozaSProperties.Models.Languages)GetValue(CLanguageProperty); }
            set { SetValue(CLanguageProperty, value); Translator.LoadDictionary(value); }
        }

        public static readonly DependencyProperty CLanguageProperty =
            DependencyProperty.Register("CLanguage", typeof(DllGrozaSProperties.Models.Languages),
                typeof(RecDroneView));
        //#region DP

        //public double Frequency
        //{
        //    get { return (double)GetValue(FrequencyProperty); }
        //    set { SetValue(FrequencyProperty, value); }
        //}

        //public static readonly DependencyProperty FrequencyProperty =
        //    DependencyProperty.Register("Frequency", typeof(double),
        //    typeof(RecDroneView));


        //public float Band
        //{
        //    get { return (float)GetValue(BandProperty); }
        //    set { SetValue(BandProperty, value); }
        //}

        //public static readonly DependencyProperty BandProperty =
        //    DependencyProperty.Register("Band", typeof(float),
        //    typeof(RecDroneView));



        //public float BearingOwn
        //{
        //    get { return (float)GetValue(BearingOwnProperty); }
        //    set { SetValue(BearingOwnProperty, value); }
        //}

        //public static readonly DependencyProperty BearingOwnProperty =
        //    DependencyProperty.Register("BearingOwn", typeof(float),
        //    typeof(RecDroneView));


        //public float BearingAnother
        //{
        //    get { return (float)GetValue(BearingAnotherProperty); }
        //    set { SetValue(BearingAnotherProperty, value); }
        //}

        //public static readonly DependencyProperty BearingAnotherProperty =
        //    DependencyProperty.Register("BearingAnother", typeof(float),
        //    typeof(RecDroneView));


        ////public float Length
        ////{
        ////    get { return (float)GetValue(LengthProperty); }
        ////    set { SetValue(LengthProperty, value); }
        ////}

        ////public static readonly DependencyProperty LengthProperty =
        ////    DependencyProperty.Register("Length", typeof(float),
        ////    typeof(RecDroneView));



        //public string Type
        //{
        //    get { return (string)GetValue(TypeProperty); }
        //    set { SetValue(TypeProperty, value); }
        //}

        //public static readonly DependencyProperty TypeProperty =
        //    DependencyProperty.Register("Type", typeof(string),
        //    typeof(RecDroneView));


        //public ImageSource Image
        //{
        //    get { return (ImageSource)GetValue(ImageProperty); }
        //    set { SetValue(ImageProperty, value); }
        //}

        //public static readonly DependencyProperty ImageProperty =
        //    DependencyProperty.Register("Image", typeof(ImageSource),
        //    typeof(RecDroneView));

        //public DateTime TimeUpdate
        //{
        //    get { return (DateTime)GetValue(TimeUpdateProperty); }
        //    set { SetValue(TimeUpdateProperty, value); }
        //}

        //public static readonly DependencyProperty TimeUpdateProperty =
        //    DependencyProperty.Register("TimeUpdate", typeof(DateTime),
        //    typeof(RecDroneView));


        //public float DistanceOwn
        //{
        //    get { return (float)GetValue(DistanceOwnProperty); }
        //    set { SetValue(DistanceOwnProperty, value); }
        //}

        //public static readonly DependencyProperty DistanceOwnProperty =
        //    DependencyProperty.Register("DistanceOwn", typeof(float),
        //    typeof(RecDroneView));


        //public float DistanceAnother
        //{
        //    get { return (float)GetValue(DistanceAnotherProperty); }
        //    set { SetValue(DistanceAnotherProperty, value); }
        //}

        //public static readonly DependencyProperty DistanceAnotherProperty =
        //    DependencyProperty.Register("DistanceAnother", typeof(float),
        //    typeof(RecDroneView));


        //public double Latitude
        //{
        //    get { return (double)GetValue(LatitudeProperty); }
        //    set { SetValue(LatitudeProperty, value); }
        //}

        //public static readonly DependencyProperty LatitudeProperty =
        //    DependencyProperty.Register("Latitude", typeof(double),
        //    typeof(RecDroneView));


        //public double Longitude
        //{
        //    get { return (double)GetValue(LongitudeProperty); }
        //    set { SetValue(LongitudeProperty, value); }
        //}

        //public static readonly DependencyProperty LongitudeProperty =
        //    DependencyProperty.Register("Longitude", typeof(double),
        //    typeof(RecDroneView));
        //#endregion


        public RecDroneView()
        {
            InitializeComponent();

            FormatViewCoordSource = CoordFormat.DD_MM_mm;

            recDroneViewModel.Drones = new ObservableCollection<RecDroneModel>();

            this.DataContext = recDroneViewModel;

            
        }


        #region Buttons 

        public DronePropertyView dronePropertyWindow;

        private void AddSourceButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                dronePropertyWindow = new DronePropertyView();

                OnIsWindowPropertyOpen?.Invoke(this, dronePropertyWindow);

                if (dronePropertyWindow.ShowDialog() == true)
                {
                    // Событие добавления одной записи
                    OnAddRecord?.Invoke(this, dronePropertyWindow.DroneProperties);
                }
            }
            catch 
            { }
        }

        private void UpdateSourceButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (recDroneViewModel.DroneSelected != null)
                {
                    dronePropertyWindow = new DronePropertyView(recDroneViewModel.DroneSelected.Clone());

                    OnIsWindowPropertyOpen?.Invoke(this, dronePropertyWindow);

                    if (dronePropertyWindow.ShowDialog() == true && !dronePropertyWindow.DroneProperties.EqualTo(new DronePropertiesModel(recDroneViewModel.DroneSelected.Clone())))
                    {
                        // Событие изменения одной записи
                        OnChangeRecord?.Invoke(this, dronePropertyWindow.DroneProperties);
                    }
                }
            }
            catch { }
        }

        private void RemoveSourceButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (recDroneViewModel.DroneSelected != null)
                {
                    var selectedDroneProperty = new DronePropertiesModel(recDroneViewModel.DroneSelected);

                    // Событие удаления одной записи
                    OnDeleteRecord?.Invoke(this, selectedDroneProperty);
                }
            }
            catch { }
        }

        private void ClearSourceButton_Click(object sender, RoutedEventArgs e)
        {
            recDroneViewModel.Drones.Clear();
            // Событие удаления всех записей
            OnClearRecords?.Invoke(this, null) ;
        }


        private void ClearTrackButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var selectedDroneProperty = new DronePropertiesModel(recDroneViewModel.Drones.Where(t => t.ID == recDroneViewModel.DroneLeftClicked.ID).FirstOrDefault());
                // Событие удаления траектории одного дрона
                OnDeleteTrackRecord?.Invoke(this, selectedDroneProperty);
            }
            catch { }
        }
        #endregion


        public void InitTypes(Dictionary<byte, string> types)
        {
            DroneTypes.UpdateTypes(types);
        }

        public event EventHandler<RecDroneModel> OnDoubleClickDrone;
        private void ListDrones_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (recDroneViewModel.DroneSelected != null)
            {
                OnDoubleClickDrone?.Invoke(this, recDroneViewModel.DroneSelected);
            }
        }

        public event EventHandler<RecDroneModel> OnSelectionChanged;
        private void ListDrones_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count != 0)
            {
                OnSelectionChanged?.Invoke(this, (RecDroneModel)e.AddedItems[0]);
            }
        }

        private void BearingkButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                OnAskBearing?.Invoke(this, recDroneViewModel.Drones.Where(t => t.ID == recDroneViewModel.DroneLeftClicked.ID).FirstOrDefault());
            }
            catch(Exception ex)
            { }
        }


        private void JammingButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int ampNum = 0;
                if ((sender as MenuItem).Name == "Amp1")
                {
                    ampNum = 1;
                    //return 10 + range;
                }

                if ((sender as MenuItem).Name == "Amp2")
                {
                    ampNum =2;
                    //return 20 + range;
                }
                var selectedDroneProperty = new DronePropertiesModel(recDroneViewModel.Drones.Where(t => t.ID == recDroneViewModel.DroneLeftClicked.ID).FirstOrDefault());
                OnSendToJamming?.Invoke(this, (ampNum, selectedDroneProperty));
            }
            catch { }
        }

        private void RSButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var selectedDroneProperty = new DronePropertiesModel(recDroneViewModel.Drones.Where(t => t.ID == recDroneViewModel.DroneLeftClicked.ID).FirstOrDefault());
                OnSendToRS?.Invoke(this, selectedDroneProperty);
            }
            catch { }
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            if (Drones != null)
            {
                OnSaveSource?.Invoke(this, Drones);
            }
        }


        ContextMenu cm;
        ListBoxItem listBoxItemForSave;

        private void ListBox_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            cm = this.FindResource("cmButton") as ContextMenu;
            listBoxItemForSave = sender as ListBoxItem;
            cm.PlacementTarget = listBoxItemForSave;
            
            recDroneViewModel.DroneLeftClicked = (sender as ListBoxItem).Content as RecDroneModel;
            cm.IsOpen = true;
            
        }

        private void CuirasseButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var selectedDroneProperty = new DronePropertiesModel(recDroneViewModel.Drones.Where(t => t.ID == recDroneViewModel.DroneLeftClicked.ID).FirstOrDefault());
                OnSendToCuirasse?.Invoke(this, selectedDroneProperty);
            }
            catch { }
        }

        private void GrozaRButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var selectedDroneProperty = new DronePropertiesModel(recDroneViewModel.Drones.Where(t => t.ID == recDroneViewModel.DroneLeftClicked.ID).FirstOrDefault());
                OnSendToGrozaR?.Invoke(this, selectedDroneProperty);
            }
            catch { }
        }
    }
}
