﻿using System;

namespace UISource
{
    public interface IModelMethods<T> where T : class
    {
        bool EqualTo(T model);

        T Clone();

        void Update(T model);
    }
}
