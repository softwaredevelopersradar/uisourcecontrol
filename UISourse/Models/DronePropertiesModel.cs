﻿using System.ComponentModel;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Windows.Controls.WpfPropertyGrid;

namespace UISource
{
    public enum EditMode
    {
        Add,
        Change
    }


    [CategoryOrder("Common", 1)]
    [CategoryOrder("Track point", 2)]
    public class DronePropertiesModel : IModelMethods<DronePropertiesModel>, INotifyPropertyChanged
    {
        public string nameCategory = "Common";
        public string nameCategory2 = "Track point";

        private int _id;
        [Browsable(false)]
        public int Id
        {
            get => _id;
            set
            {
                if (_id == value) return;
                _id = value;
                OnPropertyChanged();
            }
        }

        private EditMode _mode;
        [Browsable(false)]
        public EditMode Mode
        {
            get => _mode;
            set
            {
                if (value == _mode) return;
                _mode = value;
                OnPropertyChanged();
            }
        }

        private double _frequency;
        [Category("Common")]
        [PropertyOrder(1)]
        //[Display(Name = {DynamicUISourcePgFrequency", ResourceType = typeof(Resources))]
        //[DisplayName(nameof(Frequency))]
        public double Frequency
        {
            get => _frequency;
            set
            {
                if (_frequency == value) return;
                _frequency = value;
                OnPropertyChanged();
            }
        }

        


        private float _band;
        [Category("Common")]
        [PropertyOrder(2)]
        [DisplayName(nameof(Band))]
        public float Band
        {
            get => _band;
            set
            {
                if (_band == value) return;
                _band = value;
                OnPropertyChanged();
            }
        }


        private double _frequencyRX;
        [Category("Common")]
        [PropertyOrder(3)]
        [DisplayName(nameof(FrequencyRX))]
        public double FrequencyRX
        {
            get => _frequencyRX;
            set
            {
                if (_frequencyRX == value) return;
                _frequencyRX = value;
                OnPropertyChanged();
            }
        }


        private byte _type;
        [Category("Common")]
        [PropertyOrder(4)]
        [DisplayName(nameof(Type))]
        public byte Type
        {
            get => _type;
            set
            {
                if (_type == value) return;
                _type = value;
                OnPropertyChanged();
            }
        }

        private bool _addNewPoint;
        [Category("Common")]
        [PropertyOrder(5)]
        [Browsable(false)]
        public bool AddNewPoint
        {
            get => _addNewPoint;
            set
            {
                if (_addNewPoint == value) return;
                _addNewPoint = value;
                OnPropertyChanged();
            }
        }


        private bool _approximate = true;
        [Category("Track point")]
        [Browsable(false)]
        [PropertyOrder(6)]
        public bool Approximate
        {
            get => _approximate;
            set
            {
                if (_approximate == value) return;
                _approximate = value;
                OnPropertyChanged();
            }
        }


        private float _length;
        [Category("Track point")]
        [PropertyOrder(7)]
        [DisplayName(nameof(Length))]
        public float Length
        {
            get => _length;
            set
            {
                if (_length == value) return;
                _length = value;
                OnPropertyChanged();
            }
        }

        private float _bearingOwn = -1;
        [PropertyOrder(8)]
        [DisplayName(nameof(BearingOwn))]
        [Category("Track point")]
        public float BearingOwn
        {
            get => _bearingOwn;
            set
            {
                if (_bearingOwn == value) return;
                _bearingOwn = value;
                OnPropertyChanged();
            }
        }


        private float _bearingAnother = -1;
        [PropertyOrder(9)]
        [DisplayName(nameof(BearingAnother))]
        [Category("Track point")]
        public float BearingAnother
        {
            get => _bearingAnother;
            set
            {
                if (_bearingAnother == value) return;
                _bearingAnother = value;
                OnPropertyChanged();
            }
        }

        private double _latitude;
        [Category("Track point")]
        [PropertyOrder(10)]
        [DisplayName(nameof(Latitude))]
        public double Latitude
        {
            get => _latitude;
            set
            {
                if (_latitude == value) return;
                _latitude = value;
                OnPropertyChanged();
            }
        }

        private double _longitude;
        [Category("Track point")]
        [PropertyOrder(11)]
        [DisplayName(nameof(Longitude))]
        public double Longitude
        {
            get => _longitude;
            set
            {
                if (_longitude == value) return;
                _longitude = value;
                OnPropertyChanged();
            }
        }

        private string _note;
        [Browsable(false)]
        public string Note
        {
            get => _note;
            set
            {
                if (_note == value) return;
                _note = value;
                OnPropertyChanged();
            }
        }


        //private float _distanceOwn;
        //[Category("Track point")]
        //[PropertyOrder(11)] 
        //[DisplayName("DistanceOwn*, m")]
        //public float DistanceOwn
        //{
        //    get => _distanceOwn;
        //    set
        //    {
        //        if (_distanceOwn == value) return;
        //        _distanceOwn = value;
        //        OnPropertyChanged();
        //    }
        //}


        //private float _distanceAnother;
        //[Category("Track point")]
        //[PropertyOrder(12)]
        //[DisplayName("DistanceOwn, m")]
        //public float DistanceAnother
        //{
        //    get => _distanceAnother;
        //    set
        //    {
        //        if (_distanceAnother == value) return;
        //        _distanceAnother = value;
        //        OnPropertyChanged();
        //    }
        //}


        #region IModelMethods

        public DronePropertiesModel Clone()
        {
            return new DronePropertiesModel
            {
                Id = Id,
                Mode = Mode,
                Frequency = Frequency,
                Band = Band,
                FrequencyRX = FrequencyRX,
                Length = Length,
                Type = Type,
                AddNewPoint = AddNewPoint,
                Latitude = Latitude,
                Longitude = Longitude,
                BearingOwn = BearingOwn,
                BearingAnother = BearingAnother,
                Note = Note
            };
        }

        public bool EqualTo(DronePropertiesModel model)
        {
            return Id == model.Id
                //&& Mode == model.Mode
                && Frequency == model.Frequency
                && Band == model.Band
                && FrequencyRX == model.FrequencyRX
                && Type == model.Type
                && Length == model.Length
                && AddNewPoint == model.AddNewPoint
                && Latitude == model.Latitude
                && Longitude == model.Longitude
                && BearingOwn == model.BearingOwn
                && BearingAnother == model.BearingAnother
                & Note == model.Note;
        }

        public void Update(DronePropertiesModel model)
        {
            Frequency = model.Frequency;
            FrequencyRX = model.FrequencyRX;
            Mode = model.Mode;
            Band = model.Band;
            Type = model.Type;
            Length = model.Length;
            AddNewPoint = model.AddNewPoint;
            Latitude = model.Latitude;
            Longitude = model.Longitude;
            BearingOwn = model.BearingOwn;
            BearingAnother = model.BearingAnother;
            Note = model.Note;
        }
        #endregion

        public DronePropertiesModel()
        {
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("en");
        }


        public DronePropertiesModel(RecDroneModel recDrone)
        {
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("en");
            Id = recDrone.ID;
            Frequency = recDrone.Frequency;
            FrequencyRX = recDrone.FrequencyRX;
            Band = recDrone.Band;
            Type = DroneTypes.GetNum(recDrone.Type);
            Latitude = recDrone.Latitude;
            Longitude = recDrone.Longitude;
            Length = recDrone.DistanceOwn;
            BearingOwn = recDrone.BearingOwn;
            BearingAnother = recDrone.BearingAnother;
            Note = recDrone.Note;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));

        }
    }
}
