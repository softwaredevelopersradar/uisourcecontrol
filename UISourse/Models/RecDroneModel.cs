﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Windows.Media;

namespace UISource
{
    public class RecDroneModel: IRecDrone, INotifyPropertyChanged
    {
        private int _ID;
        public int ID
        {
            get => _ID;
            set
            {
                if (_ID == value) return;
                _ID = value;
                OnPropertyChanged();
            }
        }



        private double _frequency;
        public double Frequency
        {
            get => _frequency;
            set
            {                
                if (_frequency == value) return;
                _frequency = value;
                OnPropertyChanged();
            }
        }

        private double _frequencyRX;
        public double FrequencyRX
        {
            get => _frequencyRX;
            set
            {
                if (_frequencyRX == value) return;
                _frequencyRX = value;
                OnPropertyChanged();
            }
        }


        private float _band;
        public float Band
        {
            get => _band;
            set
            {
                if (_band == value) return;
                _band = value;
                OnPropertyChanged();
            }
        }

        private string _type;
        public string Type
        {
            get => _type;
            set
            {
                if (_type == value) return;
                _type = value;
                OnPropertyChanged();
            }
        }

        private TypeBelong _typeRSM;
        public TypeBelong TypeRSM
        {
            get { return _typeRSM; }
            set
            {
                if (_typeRSM.Equals(value)) return;
                _typeRSM = value;
                OnPropertyChanged();
            }

        }

        //private float _length;
        //public float Length
        //{
        //    get => _length;
        //    set
        //    {
        //        if (_length == value) return;
        //        _length = value;
        //        OnPropertyChanged();
        //    }
        //}

        private float _distanceOwn;
        public float DistanceOwn
        {
            get => _distanceOwn;
            set
            {
                if (_distanceOwn == value) return;
                _distanceOwn = value;
                OnPropertyChanged();
            }
        }


        private float _distanceAnother;
        public float DistanceAnother
        {
            get => _distanceAnother;
            set
            {
                if (_distanceAnother == value) return;
                _distanceAnother = value;
                OnPropertyChanged();
            }
        }

        private ImageSource _image;
        public ImageSource Image
        {
            get => _image;
            set
            {
                if (_image == value) return;
                _image = value;
                OnPropertyChanged();
            }
        }


        private double _latitude;
        public double Latitude
        {
            get => _latitude;
            set
            {
                if (_latitude == value) return;
                _latitude = value;
                OnPropertyChanged();
            }
        }

        private double _longitude;
        public double Longitude
        {
            get => _longitude;
            set
            {
                if (_longitude == value) return;
                _longitude = value;
                OnPropertyChanged();
            }
        }


        private float _bearingOwn;
        public float BearingOwn
        {
            get => _bearingOwn;
            set
            {
                if (_bearingOwn == value) return;
                _bearingOwn = value;
                OnPropertyChanged();
            }
        }


        private float _bearingAnother;
        public float BearingAnother
        {
            get => _bearingAnother;
            set
            {
                if (_bearingAnother == value) return;
                _bearingAnother = value;
                OnPropertyChanged();
            }
        }

        private DateTime _timeUpdate;
        public DateTime TimeUpdate
        {
            get => _timeUpdate;
            set
            {
                if (_timeUpdate == value) return;
                _timeUpdate = value;
                OnPropertyChanged();
            }
        }


        private CoordFormat _formatCoord = CoordFormat.DD;
        public CoordFormat FormatCoord
        {
            get
            {
                return _formatCoord;
            }
            set
            {
                _formatCoord = value;
                OnPropertyChanged();
            }
        }

        private string _note;
        public string Note
        {
            get => _note;
            set
            {
                if (_note == value) return;
                _note = value;
                OnPropertyChanged();
            }
        }



        //public string LatitudePos { get; private set; }

        //public string LongitudePos { get; private set; }

        #region IModelMethods

        public RecDroneModel Clone()
        {
            return new RecDroneModel
            {
                ID = ID,
                Frequency = Frequency,
                FrequencyRX = FrequencyRX,
                Band = Band,
                Type = Type,
                TypeRSM = TypeRSM,
                DistanceOwn = DistanceOwn,
                DistanceAnother = DistanceAnother,
                Image = Image,
                Latitude = Latitude,
                Longitude = Longitude,
                BearingOwn = BearingOwn,
                BearingAnother = BearingAnother,
                TimeUpdate = TimeUpdate,
                FormatCoord = FormatCoord,
                Note = Note
            };
        }

        public bool EqualTo(RecDroneModel model)
        {
            return ID == model.ID
                && Frequency == model.Frequency
                && FrequencyRX == model.FrequencyRX
                && Band == model.Band
                && Type == model.Type
                && TypeRSM == model.TypeRSM
                && DistanceOwn == model.DistanceOwn
                && DistanceAnother == model.DistanceAnother
                && Image == model.Image
                && Latitude == model.Latitude
                && Longitude == model.Longitude
                && BearingOwn == model.BearingOwn
                && BearingAnother == model.BearingAnother
                && TimeUpdate == model.TimeUpdate
                && FormatCoord == model.FormatCoord
                && Note == model.Note;
        }

        public void Update(RecDroneModel model)
        {
            Frequency = model.Frequency;
            FrequencyRX = model.FrequencyRX;
            Band = model.Band;
            Type = model.Type;
            TypeRSM = model.TypeRSM;
            DistanceOwn = model.DistanceOwn;
            DistanceAnother = model.DistanceAnother;
            Image = model.Image;
            Latitude = model.Latitude;
            Longitude = model.Longitude;
            BearingOwn = model.BearingOwn;
            BearingAnother = model.BearingAnother;
            TimeUpdate = model.TimeUpdate;
            FormatCoord = model.FormatCoord;
            Note = model.Note;
        }
        #endregion

        public RecDroneModel()
        {
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("en");          
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));

        }



        
    }
}
