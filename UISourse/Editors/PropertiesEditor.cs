﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls.WpfPropertyGrid;

namespace UISource
{
    public class PropertiesEditor : PropertyEditor
    {
        Dictionary<string, string> dictKeyDataTemplate = new Dictionary<string, string>()
        {
            { nameof(DronePropertiesModel.Frequency), "FreqEditorKey" },
            { nameof(DronePropertiesModel.FrequencyRX), "FreqEditorKey" },
            { nameof(DronePropertiesModel.Band), "BandEditorKey" },
            { nameof(DronePropertiesModel.Type), "TypeEditorKey" },
            //{ nameof(DronePropertiesModel.AddNewPoint), "AddPointEditorKey" },
            { nameof(DronePropertiesModel.Latitude), "LatitudeEditorKey" },
            { nameof(DronePropertiesModel.Longitude), "LongitudeEditorKey"},
            { nameof(DronePropertiesModel.BearingOwn), "BearingEditorKey"},
            { nameof(DronePropertiesModel.BearingAnother), "BearingAnotherEditorKey"},
            { nameof(DronePropertiesModel.Length), "DistanceEditorKey"}
            //{ nameof(DronePropertiesModel.Approximate), "ApproximateEditorKey"}
            //{ typeof(DronePropertiesModel), "RadioIntelegenceEditorKey" }
        };

        public PropertiesEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/UISource;component/Themes/PropertyGridEditor.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            this.InlineTemplate = resource[dictKeyDataTemplate[PropertyName]];
        }

    }
}
