﻿public enum TypeBelong : byte
{
    FRIEND = 0,
    ENEMY = 1,
    UNCERTAIN = 2,
    EMPTY = 3,
    PROCESS = 4,
    
}