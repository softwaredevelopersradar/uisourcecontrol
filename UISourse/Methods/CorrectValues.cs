﻿

namespace UISource
{
    public class CorrectValues
    {
        public static void IsCorrectFreq(DronePropertiesModel properties)
        {
            if (properties.Frequency < 100.0F) { properties.Frequency = 100.0F; }
            if (properties.Frequency > 6000.0F) { properties.Frequency = 6000.0F; }
        }


        public static void IsCorrectBand(DronePropertiesModel properties)
        {
            if (properties.Band < 1) { properties.Band = 1; }
            if (properties.Band > 100) { properties.Band = 100; }
        }


        public static void IsCorrectDistance(DronePropertiesModel properties)
        {
            //if (properties.DistanceOwn < 0) { properties.DistanceOwn = 0; }
            //if (properties.DistanceOwn > 50000) { properties.DistanceOwn = 50000; }
        }

        public static void IsCorrectType(DronePropertiesModel properties)
        {
            if (properties.Type == null) { properties.Type = 0; }
        }
    }
}
