﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace UISource
{
    public class DronePropertyViewModel : ViewModelBase
    {
        //public DronePropertiesModel DroneProperties { get; private set; }
        private DronePropertiesModel _selectedProperties;
        public DronePropertiesModel SelectedProperties
        {
            get
            {
                return _selectedProperties;
            }
            set
            {
                _selectedProperties = value;
                OnPropertyChanged();
            }
        }
    }
}
