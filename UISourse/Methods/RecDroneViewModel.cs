﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;

namespace UISource
{
    public class RecDroneViewModel : DependencyObject, INotifyPropertyChanged
    {

        private RecDroneModel _droneSelected;
        public RecDroneModel DroneSelected
        {
            get
            {
                return _droneSelected;
            }
            set
            {
                _droneSelected = value;
                OnPropertyChanged();
            }
        }


        private RecDroneModel _droneLeftClicked;
        public RecDroneModel DroneLeftClicked
        {
            get
            {
                return _droneLeftClicked;
            }
            set
            {
                _droneLeftClicked = value;
                OnPropertyChanged();
            }
        }


        public ObservableCollection<RecDroneModel> Drones { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));

        }
    }
}
