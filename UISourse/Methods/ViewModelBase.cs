﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace UISource
{
    public abstract class ViewModelBase : INotifyPropertyChanged, IDisposable
    {
        protected ViewModelBase()
        { }


        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));

        }

        public void Dispose()
        {
            OnDispose();
        }

        protected virtual void OnDispose()
        { }
    }
}
