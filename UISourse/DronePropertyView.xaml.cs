﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls.WpfPropertyGrid;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace UISource
{
    /// <summary>
    /// Логика взаимодействия для DronePropertyView.xaml
    /// </summary>
    public partial class DronePropertyView : Window
    {
        public DronePropertiesModel DroneProperties { get; private set; }
        //private DronePropertyViewModel PropertiesViewModel = new DronePropertyViewModel();

        public DronePropertyView()
        {
            InitializeComponent();

            InitEditors();

            DroneProperties = new DronePropertiesModel() { Mode = EditMode.Add, AddNewPoint = true};
            propertyGrid.SelectedObject = DroneProperties;
            //DroneProperties.PropertyChanged += PropertyChanged;
            Title = SMeaning.meaningAddRecord;
            Icon = new BitmapImage(new Uri(@"pack://application:,,,/"
                                    + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                                    + ";component/"
                                    + "Images/AddRec.ico", UriKind.Absolute));
            InitProperty();
        }


        public DronePropertyView(RecDroneModel recDrone)
        {
            InitializeComponent();
            
            InitEditors();
            DroneProperties = new DronePropertiesModel(recDrone) { Mode = EditMode.Change};
            propertyGrid.SelectedObject = DroneProperties;
            //DroneProperties.PropertyChanged += PropertyChanged;
            Title = SMeaning.meaningChangeRecord;
            Icon = new BitmapImage(new Uri(@"pack://application:,,,/"
                                    + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                                    + ";component/"
                                    + "Images/ChangeRec.ico", UriKind.Absolute));
            InitProperty();
        }


        private void InitEditors()
        {
            
            //propertyGrid.Editors.Add(new PropertiesEditor(nameof(DroneProperties), typeof(DronePropertiesModel)));
            propertyGrid.Editors.Add(new PropertiesEditor(nameof(DroneProperties.Frequency), typeof(DronePropertiesModel)));
            propertyGrid.Editors.Add(new PropertiesEditor(nameof(DroneProperties.FrequencyRX), typeof(DronePropertiesModel)));
            propertyGrid.Editors.Add(new PropertiesEditor(nameof(DroneProperties.Band), typeof(DronePropertiesModel)));
            propertyGrid.Editors.Add(new PropertiesEditor(nameof(DroneProperties.Type), typeof(DronePropertiesModel)));
            //propertyGrid.Editors.Add(new PropertiesEditor(nameof(DroneProperties.AddNewPoint), typeof(DronePropertiesModel)));
            propertyGrid.Editors.Add(new PropertiesEditor(nameof(DroneProperties.Latitude), typeof(DronePropertiesModel)));
            propertyGrid.Editors.Add(new PropertiesEditor(nameof(DroneProperties.Longitude), typeof(DronePropertiesModel)));
            //propertyGrid.Editors.Add(new PropertiesEditor(nameof(DroneProperties.Approximate), typeof(DronePropertiesModel)));
            propertyGrid.Editors.Add(new PropertiesEditor(nameof(DroneProperties.BearingOwn), typeof(DronePropertiesModel)));
            propertyGrid.Editors.Add(new PropertiesEditor(nameof(DroneProperties.BearingAnother), typeof(DronePropertiesModel)));
            propertyGrid.Editors.Add(new PropertiesEditor(nameof(DroneProperties.Length), typeof(DronePropertiesModel)));
        }


        //private void PropertyChanged(object sender, PropertyChangedEventArgs e)
        //{
        //    //switch (e.PropertyName)
        //    //{
        //    //    case nameof(DroneProperties.Approximate):
        //    //        propertyGrid.Properties[nameof(DroneProperties.BearingAnother)].IsReadOnly = DroneProperties.Approximate;
        //    //        break;
        //    //    default:
        //    //        break;
        //    //}

        //}


        #region PropertyException

        private void InitProperty()
        {
            foreach (var property in propertyGrid.Properties)
            {
                try
                {
                    if (property.IsBrowsable == false) { continue; }

                    if (property.PropertyValue.SubProperties.Count != 0)
                    {
                        foreach (var subProperty in property.PropertyValue.SubProperties)
                            subProperty.PropertyValue.PropertyValueException += PropertyGridSubException;
                        continue;
                    }
                    property.PropertyValue.PropertyValueException += PropertyGridException;
                }
                catch (Exception ex)
                { MessageBox.Show(ex.Message); }
            }
        }
        
        private void PropertyGridSubException(object sender, ValueExceptionEventArgs e)
        {
            propertyGrid.Properties[(sender as PropertyItemValue).ParentProperty.ParentValue.ParentProperty.Name].SetValue((sender as PropertyItemValue).Value);
        }

        private void PropertyGridException(object sender, ValueExceptionEventArgs e)
        {
            propertyGrid.Properties[(sender as PropertyItemValue).ParentProperty.Name].SetValue((sender as PropertyItemValue).Value);
        }
        #endregion

        //private void ButtonNoApply_Click(object sender, RoutedEventArgs e)
        //{
        //    DialogResult = false;
        //}

        private void ButtonApply_Click(object sender, RoutedEventArgs e)
        {
            if (IsAddClick((DronePropertiesModel)propertyGrid.SelectedObject) != null)
            {
                DialogResult = true;
            }
        }

        public DronePropertiesModel IsAddClick(DronePropertiesModel PropertiesWindow)
        {
            CorrectValues.IsCorrectFreq(PropertiesWindow);
            CorrectValues.IsCorrectBand(PropertiesWindow);
            //CorrectValues.IsCorrectDistance(PropertiesWindow);
            CorrectValues.IsCorrectType(PropertiesWindow);
            return PropertiesWindow;
        }

        private void gridProperty_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (IsAddClick((DronePropertiesModel)propertyGrid.SelectedObject) != null)
                {
                    DialogResult = true;
                }
            }
        }


        public void SetLanguagePropertyGrid(DllGrozaSProperties.Models.Languages language)
        {
            LoadTranslatorPropertyGrid(language);
            Translator.ChangeLanguagePropertyGrid(language, propertyGrid);
        }

        private void LoadTranslatorPropertyGrid(DllGrozaSProperties.Models.Languages language)
        {
            ResourceDictionary dict = new ResourceDictionary();
            try
            {
                switch (language)
                {
                    case DllGrozaSProperties.Models.Languages.EN:
                        dict.Source = new Uri("/GrozaS_AWS;component/Languages/UISource/StringResource.EN.xaml",
                                      UriKind.Relative);
                        break;
                    case DllGrozaSProperties.Models.Languages.RU:
                        dict.Source = new Uri("/GrozaS_AWS;component/Languages/UISource/StringResource.RU.xaml",
                                            UriKind.Relative);
                        break;
                    case DllGrozaSProperties.Models.Languages.AZ:
                        dict.Source = new Uri("/GrozaS_AWS;component/Languages/UISource/StringResource.AZ.xaml",
                            UriKind.Relative);
                        break;
                    case DllGrozaSProperties.Models.Languages.SR:
                        dict.Source = new Uri("/GrozaS_AWS;component/Languages/UISource/StringResource.SR.xaml",
                            UriKind.Relative);
                        break;
                    default:
                        dict.Source = new Uri("/GrozaS_AWS;component/Languages/UISource/StringResource.RU.xaml",
                                          UriKind.Relative);
                        break;
                }

                this.Resources.MergedDictionaries.Add(dict);
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
        }

        //#region Language
        //Dictionary<string, string> TranslateDic;

        //private void ChangeLanguage(Languages newLanguage)
        //{
        //    SetDynamicResources(newLanguage);
        //    LoadDictionary();
        //    SetCategoryGlobalNames();
        //    SetCategoryLocalNames();
        //}
        //private void SetDynamicResources(Languages newLanguage)
        //{
        //    try
        //    {
        //        ResourceDictionary dict = new ResourceDictionary();
        //        switch (newLanguage)
        //        {
        //            case Languages.EN:
        //                dict.Source = new Uri("/DllGrozaSProperties;component/Languages/StringResource.EN.xaml",
        //                              UriKind.Relative);
        //                break;
        //            case Languages.RU:
        //                dict.Source = new Uri("/DllGrozaSProperties;component/Languages/StringResource.RU.xaml",
        //                                   UriKind.Relative);
        //                break;
        //            default:
        //                dict.Source = new Uri("/DllGrozaSProperties;component/Languages/StringResource.EN.xaml",
        //                                  UriKind.Relative);


        //                break;
        //        }
        //        Resources.MergedDictionaries.Add(dict);
        //    }
        //    catch (Exception ex)
        //    {
        //        //TODO
        //    }

        //}

        //void LoadDictionary()
        //{
        //    var translation = Properties.Resources.Translation;
        //    XmlDocument xDoc = new XmlDocument();
        //    xDoc.LoadXml(translation);
        //    TranslateDic = new Dictionary<string, string>();
        //    // получим корневой элемент
        //    XmlElement xRoot = xDoc.DocumentElement;
        //    foreach (XmlNode x2Node in xRoot.ChildNodes)
        //    {
        //        if (x2Node.NodeType == XmlNodeType.Comment)
        //            continue;

        //        // получаем атрибут ID
        //        if (x2Node.Attributes.Count > 0)
        //        {
        //            XmlNode attr = x2Node.Attributes.GetNamedItem("ID");
        //            if (attr != null)
        //            {
        //                foreach (XmlNode childnode in x2Node.ChildNodes)
        //                {
        //                    // если узел - language
        //                    if (childnode.Name == Local.General.Language.ToString())
        //                    {
        //                        if (!TranslateDic.ContainsKey(attr.Value))
        //                            TranslateDic.Add(attr.Value, childnode.InnerText);
        //                    }

        //                }
        //            }
        //        }
        //    }
        //}

        //private void SetCategoryGlobalNames()
        //{
        //    try
        //    {
        //        foreach (var nameCategory in PropertyGlobal.Categories.Select(category => category.Name).ToList())
        //        {
        //            var prop = PropertyGlobal.Categories.FirstOrDefault(t => t.Name == nameCategory);
        //            if (prop != null && TranslateDic.ContainsKey(nameCategory))
        //                prop.HeaderCategoryName = TranslateDic[nameCategory];
        //        }
        //    }
        //    catch (Exception)
        //    {

        //    }
        //}

        //private void SetCategoryLocalNames()
        //{
        //    try
        //    {
        //        foreach (var nameCategory in PropertyLocal.Categories.Select(category => category.Name).ToList())
        //            PropertyLocal.Categories.First(t => t.Name == nameCategory).HeaderCategoryName = TranslateDic[nameCategory];
        //    }
        //    catch (Exception)
        //    {

        //    }
        //}
        //#endregion
    }
}
