﻿using System;

using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace UISource
{
    public class TypeBelongColorConverter : IValueConverter
    {
        
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Color cColor = new Color();

            switch ((TypeBelong)value)
            {
                case TypeBelong.EMPTY:
                    cColor = (Color)System.Windows.Media.ColorConverter.ConvertFromString("#acc4ce");
                    break;

                case TypeBelong.ENEMY:
                    cColor = (Color)System.Windows.Media.ColorConverter.ConvertFromString("#FF6347");
                    break;

                case TypeBelong.FRIEND:
                    cColor = (Color)System.Windows.Media.ColorConverter.ConvertFromString("#FF00D7F9"); 
                    //cColor = (Color)System.Windows.Media.ColorConverter.ConvertFromString("#0ccd1b");
                    //cColor = (Color)System.Windows.Media.ColorConverter.ConvertFromString("#8AF934");
                    //cColor = (Color)System.Windows.Media.ColorConverter.ConvertFromString("#66CDAA"); //#A1DD73
                    break;

                case TypeBelong.PROCESS:
                    cColor = (Color)System.Windows.Media.ColorConverter.ConvertFromString("#acc4ce");
                    break;

                case TypeBelong.UNCERTAIN:
                    cColor = (Color)System.Windows.Media.ColorConverter.ConvertFromString("#acc4ce");  //#e49b0f
                    break;

                default:                    
                    break;
            }
            return new SolidColorBrush(cColor);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return  (TypeBelong)Enum.Parse(typeof(TypeBelong), (string)value);           
        }
    
}
}
