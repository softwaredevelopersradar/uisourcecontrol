﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace UISource
{
    public class TypeBelongStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
           
            if ((TypeBelong)value == TypeBelong.EMPTY)
                return "";
            if ((TypeBelong)value == TypeBelong.PROCESS)
                return "?";
            if ((TypeBelong)value == TypeBelong.UNCERTAIN)
                return "NO SIGNAL";
            else
                return Enum.GetName(typeof(TypeBelong), value); 
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return  (TypeBelong)Enum.Parse(typeof(TypeBelong), (string)value);           
        }
    
}
}
