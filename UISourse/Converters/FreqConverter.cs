﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace UISource
{
    public class FreqConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool bTemp = value.ToString().Contains(".");
            int Freq = 0;

            if (bTemp) { Freq = (int)((double)(value) * 10d); }
            else Freq = System.Convert.ToInt32((double)value * 10);

            string strFreqResult = "";
            string strFreq = System.Convert.ToString(Freq);
            int iLength = strFreq.Length;

            try
            {
                if (strFreq == "0")
                    return string.Empty;

                if (iLength == 5)
                {
                    strFreqResult = strFreq.Substring(0, 1) + " " + strFreq.Substring(1, 3) + ".".ToString(culture) + strFreq.Substring(iLength - 1, 1);
                }
                else
                {
                    strFreqResult = strFreq.Substring(0, iLength - 1) + ".".ToString(culture) + strFreq.Substring(iLength - 1, 1);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }

            return strFreqResult;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //double result;

            //if (Double.TryParse(value.ToString(), NumberStyles.Any, culture, out result))
            //{
            //    return result;
            //}

            Int32 result;

            if (Int32.TryParse(value.ToString(), NumberStyles.Any, culture, out result))
            {
                return result;
            }
            return value;
        }
    }
}
