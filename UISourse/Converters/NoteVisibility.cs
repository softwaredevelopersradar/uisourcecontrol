﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace UISource
{
    public class NoteVisibility : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Visibility visibility = Visibility.Hidden;

            if ((TypeBelong)value == TypeBelong.FRIEND)
            {
                visibility = Visibility.Visible;
            }
            return visibility;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
}
