﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace UISource
{
    public class NoteConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var input = (string)value;
            if(input == null) return string.Empty;
            if(input.Length > 15 && input.Substring(14,1) == ":")
                return input.Substring(0,14);
            return string.Empty;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (TypeBelong)Enum.Parse(typeof(TypeBelong), (string)value);
        }

    }
}
