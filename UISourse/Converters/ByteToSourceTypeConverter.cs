﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace UISource
{
    public class ByteToSourceTypeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var t = (byte)value;
            return DroneTypes.GetType(t);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var s = (string)value;
            return DroneTypes.GetNum(s);
        }
    }
}
