﻿using System;

using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace UISource
{
    public class TypeNameColorConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Color cColor = new Color();

            switch ((TypeBelong)value)
            {
                case TypeBelong.EMPTY:
                    cColor = (Color)System.Windows.Media.ColorConverter.ConvertFromString("#FF00D7F9");
                    break;

                case TypeBelong.ENEMY:
                    cColor = (Color)System.Windows.Media.ColorConverter.ConvertFromString("#FFFF6347");
                    break;

                case TypeBelong.FRIEND:
                    cColor = (Color)System.Windows.Media.ColorConverter.ConvertFromString("#FF00D7F9"); 
                    //cColor = (Color)System.Windows.Media.ColorConverter.ConvertFromString("#0ccd1b");
                    //cColor = (Color)System.Windows.Media.ColorConverter.ConvertFromString("#8AF934");
                    //cColor = (Color)System.Windows.Media.ColorConverter.ConvertFromString("#66CDAA");
                    break;

                case TypeBelong.PROCESS:
                    cColor = (Color)System.Windows.Media.ColorConverter.ConvertFromString("#FF00D7F9");
                    break;

                case TypeBelong.UNCERTAIN:
                    cColor = (Color)System.Windows.Media.ColorConverter.ConvertFromString("#FFacc4ce");
                    break;

                default:                    
                    break;
            }
            return new SolidColorBrush(cColor);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return  (TypeBelong)Enum.Parse(typeof(TypeBelong), (string)value);           
        }
    
}
}
