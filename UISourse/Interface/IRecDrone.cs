﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;

namespace UISource
{
    interface IRecDrone
    {
        #region Properties

        double Frequency { get; set; }
        float Band { get; set; }

        string Type { get; set; }
        ImageSource Image { get; set; }

        #endregion


    }
}
