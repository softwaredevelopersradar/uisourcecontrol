﻿
namespace UISource
{
    interface IDroneProperties 
    {
        #region Properties

        double Frequency { get; set; }
        float Band { get; set; }

        string Type { get; set; }
        bool AddNewPoint { get; set; }
        double Latitude { get; set; }
        double Longitude { get; set; }

        float BearingOwn { get; set; }
        //float DistanceOwn { get; set; }
        #endregion
    }
}
